# Overlord bot proof of concept

A bot that listens to mic input for Bogey Dope calls, reads a [TacScribe](https://gitlab.com/overlord-bot/tac-scribe/tree/implementation)
database and speaks the response.

Since this is just a proof of concept it is just aimed at devs and people
who want to mess around.

It requires the pilots in the DCS server to have the PILOT NAME in the format
of "GROUP 1-1" where GROUP is a group callsign like "Uzi", "Dolt" etc. and
1-1 is the unique identifier within that group. e.g. the following names are
all valid.

DOLT 1-1 | ScAvenger
UZI 2-3 | RurouniJones
SPRINGFIELD 1-3 | StandingCow

Note that the GROUP *MUST* be in capital letters. the | is optional (but 
it looks nice so use it!).

Bogey Dope calls must be spoken using the following formatn:

`Overlord, UZI 1-2, request bogey dope`

Also Note that because there has been no voice training you need to speak
slowly and clearly to help understanding. Some GROUP Ids are more easily
recognised than others.

## Development

### Known issues

* Doesn't refresh the auth token so will fail eventually
* Only listens to local mic
* Hardcoded database information. Change this if you want to test
* Requires access to the LUIS app which is not shared. Ask if you want
  access
* API Keys for various things not included. As before, ask if you want
  access

