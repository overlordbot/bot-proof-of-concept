﻿using System;

namespace SpeechTest
{
    class Program
    {
        static AudioListener listener = new AudioListener();

        static void Main(string[] args)
        {
            Console.WriteLine("Initializing");
            listener.RecognizeIntentAsync().Wait();
            Console.WriteLine("Please press Enter to continue.");
            Console.ReadLine();
        }
    }
}
