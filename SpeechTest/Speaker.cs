﻿using Microsoft.CognitiveServices.Speech;
using System;
using System.Threading.Tasks;

namespace SpeechTest
{
    class Speaker
    {
        private static SpeechConfig speechConfig = SpeechConfig.FromSubscription("FILL_ME_IN", "FILL_ME_IN");
        private static SpeechSynthesizer synthesizer = new SpeechSynthesizer(speechConfig);

        public static async Task SendResponse(string text)
        {
            Console.WriteLine($"Speech synthesized to speaker for text [{text}]");
            using (var textresult = await synthesizer.SpeakTextAsync(text))
            {
                if (textresult.Reason == ResultReason.SynthesizingAudioCompleted)
                {
                    // No-op
                }
                else if (textresult.Reason == ResultReason.Canceled)
                {
                    var cancellation = SpeechSynthesisCancellationDetails.FromResult(textresult);
                    Console.WriteLine($"CANCELED: Reason={cancellation.Reason}");

                    if (cancellation.Reason == CancellationReason.Error)
                    {
                        Console.WriteLine($"CANCELED: ErrorCode={cancellation.ErrorCode}");
                        Console.WriteLine($"CANCELED: ErrorDetails=[{cancellation.ErrorDetails}]");
                        Console.WriteLine($"CANCELED: Did you update the subscription info?");
                    }
                }
            } 
        }
    }
}
